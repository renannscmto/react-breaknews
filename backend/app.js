const express = require("express");
const cors = require("cors");
const path = require("path")
const router = require("./router");

const app = express();

// Permite que todas as solicitações feitas à partir desta URL não sejam bloqueadas.
const options = {
    origin: 'http://localhost:3000',
};
app.use(cors(options))

app.use(express.json())
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use(router)

module.exports = app;
