const { Sequelize } = require("sequelize");
require("dotenv").config();

const sequelize = new Sequelize(process.env.NAME_DATABASE, process.env.USER_DATABASE, process.env.PASS_DATABASE, {
  host: process.env.HOST_DATABASE,
  dialect: process.env.TYPE_DATABASE,
  port: process.env.PORT_DATABASE,
});

module.exports = sequelize;
