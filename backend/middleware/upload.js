const multer = require("multer")

module.exports = multer({
    // diskStorage permite manipular locar para salvar a imagem.
    storage: multer.diskStorage({
        // Local para salvar a imagem.
        destination: (req, file, callback) => {
            callback(null, './uploads/');
        },
        // Nome que deve ser atribuido ao arquivo.
        filename: (req, file, callback) => {
            callback(null, Date.now().toString() + "_" + file.originalname);
        }
    }),

    // Valida as extenções dos arquivos.
    fileFilter: (req, file, callback) => {
        // Verifica se a extenção do arquivo está dentro do array de arquivos válidos.
        const extension = ['image/png', 'image/jpg', 'image/jpeg'].find(extensionCheck => extensionCheck == file.mimetype);

        if (!extension) {
            callback(null, false);
        } else {
            callback(null, true);
        }
    }
})
