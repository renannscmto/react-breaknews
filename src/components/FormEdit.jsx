import React from "react";

export default function FormEdit(props) {
    const {setUpdateTitle, setUpdateText, updateTitle, updateText, handleSendUpdate} = props;

    return (
        <div className="edit-form">
            <div>
                <label htmlFor="title">Dígite o Title da nova news:</label>
                <input
                    type="text"
                    name="title"
                    placeholder="Novo título"
                    value={updateTitle}
                    onChange={(e) => setUpdateTitle(e.target.value)}
                    />
            </div> 
            <div>
                <label htmlFor="news">Dígite o Text da nova news:</label>
                <input
                    type="text"
                    name="news"
                    placeholder="Novo texto"
                    value={updateText}
                    onChange={(e) => setUpdateText(e.target.value)}
                    />
            </div>
            <button onClick={handleSendUpdate}>Enviar</button>
        </div>
    )
}