const app = require("./app");
require("dotenv").config(); 

app.listen(process.env.PORT_SERVER, 'localhost');
app.on('listening', function() {
    console.log(`Server running port: ${process.env.PORT_SERVER}`); 
});