const modelsNews = require("../models/modelsNews")

const getNews = async (_req, res) => {
    try {
        const users = await modelsNews.getNews();
        return res.status(200).json(users);
    } catch (error) {
        return error
    }
};

const putNews = async (req, res) => {
    const {id} = req.params

    const update = await modelsNews.updateNews(id, req.body)
    return res.status(204).json(update)
}

const postNews = async (req, res) => {
    const createNews = await modelsNews.postNews(req.body)
    return res.status(201).json(createNews)
}

const deleteNews = async (req, res) => {
    const { id } = req.params

    await modelsNews.deleteNews(id)
    return res.status(200).json({message: `deleted news: ${id}`})
}

const uploadImage = async (req, res) => {
    const {filename} = req.file
    const {title, news} = req.body

    await modelsNews.uploadImage(filename, title, news)
    return res.status(200).json({message: filename})
}

module.exports = {
    getNews,
    putNews,
    postNews,
    deleteNews,
    uploadImage
}