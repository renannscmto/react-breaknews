import {Link} from 'react-router-dom';
import Logo from '../assets/logo.png'

export default function Navbar() {
    return (
        <nav className='container-nav'>
            <div className='box-nav'>
                <div className='box-logo'>
                   <img src={Logo} alt="Imagem logo page" />
                </div>
                <ul>
                    <li> <Link to="/">Home</Link> </li>
                    <li> <Link to="/get">View</Link> </li>
                    <li> <Link to="/post">Send</Link> </li>
                </ul>
            </div>
        </nav>
    )
}