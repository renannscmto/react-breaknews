const { QueryTypes } = require('sequelize');
const sequelize = require("./db");

const getNews = async _ => {
    try {
        const users = await sequelize.query("SELECT * FROM `news`", { type: QueryTypes.SELECT });
        return users;
    } catch (error) {
        console.log("Error ao buscar dados: ", error);
        return error;
    };
};

const updateNews = async (id, datas) => {
    const {title, news} = datas;
    const query = "UPDATE news SET title = :title, news = :news WHERE id_new = :id";
  
    try {
        const upadateNews = await sequelize.query(query, { replacements: { id, title, news }, type: QueryTypes.UPDATE });
        return upadateNews;
    } catch (error) {
        console.log("Error ao atualizar os dados: ", error.message);
        return error;
    };
}

const deleteNews = async (id) => {
    const query = "DELETE FROM news WHERE id_new = :id";
    
    try {
        const deleteNews = await sequelize.query(query, { replacements: { id: id }, type: QueryTypes.DELETE });
        return deleteNews;
    } catch (error) {
        console.log("Error ao deletar dados: ", error);
        return error;
    };
}

const postNews = async (breaknews) => {
    const {title, news} = breaknews;
    const query = "INSERT INTO news (title, news) VALUES (:title, :news)";

    try {
        const insertNews = await sequelize.query(query, { replacements: { title, news }, type: QueryTypes.INSERT });
        return insertNews;
    } catch (error) {
        console.log("Error ao inserir dados: ", error);
        return error;
    };
}

const uploadImage = async (image, title, news) => {
    const query = "INSERT INTO news (title, news, image) VALUES (:title, :news, :image)";

    try {
        const upload = await sequelize.query(query, {replacements: { title, news,   image: image }, type: QueryTypes.INSERT});
        return upload;
    } catch (error) {
        console.log("Erro ao enviar arquivo para o banco de dados.", error.message);
        return error.message;
    };
}

module.exports = {
    getNews,
    updateNews,
    postNews,
    deleteNews,
    uploadImage,
}