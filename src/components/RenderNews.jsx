import React from "react";

export default function RenderNews({data, handleDeleteNews, handleUpdateNews}) {
    return (
        data.map(item => (
            <div key={item.id_new} className="grid-item">
                <img src="../../backend/uploads/1709565348698_coroa-flores.png" alt="Imagem da notícia" />
                <h3>{item.title}</h3>
                <p>{item.news}</p>
                <div className="container-btn-actions">
                    <button className="btn-delete" onClick={() => handleDeleteNews(item.id_new)}>
                        <i className="fa fa-trash" aria-hidden="true"></i> Delete
                    </button>

                    <button className="btn-update" onClick={() => handleUpdateNews(item.id_new, item.title, item.news)}>
                        <i className="fa fa-edit" aria-hidden="true"></i> Edit
                    </button>
                </div>
                <small>Criado em: {item.date_create}</small>
            </div>
        ))
    )
}