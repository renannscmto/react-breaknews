import React from "react";

export default function FormInsert(props) {
    const {handleSendDataImage, handleDataImage, handleDataTitle, handleDataNews, title, news} = props;
    
    return (
        <form className="container-form" onSubmit={handleSendDataImage} encType="multipart/form-data">
            <div className="box-form">
                <div className="box-image">
                    <label htmlFor="image">Escolha a nova imagem:</label>
                    <input type="file" name="image" onChange={handleDataImage} />
                </div>
                <div className="box-title">
                    <label htmlFor="title">Dígite o Title da nova news:</label>
                    <input type="text" name="title" value={title} onChange={handleDataTitle} placeholder="Title..."/>
                </div>
                <div className="box-news">
                    <label htmlFor="news">Dígite o Text da nova news:</label>
                    <input type="text" name="news" value={news} onChange={handleDataNews} placeholder="Text..."/>
                </div>

                <button type="submit">Send</button>
            </div>
        </form>
    )
}