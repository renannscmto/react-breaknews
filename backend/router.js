const express = require("express");
const router = express.Router();

const controllerRouter = require("./controller/controllerRouter")
const middlewareNews = require("./middleware/middlewareNews")
const middlewareUpload = require("./middleware/upload")

router.get("/news", controllerRouter.getNews);

router.put("/news/:id", 
        middlewareNews.validateUpdateTitle, 
        middlewareNews.validateUpdateText,
        controllerRouter.putNews);

router.post("/news", 
        middlewareNews.validateUpdateTitle, 
        middlewareNews.validateUpdateText, 
        controllerRouter.postNews)

router.delete("/news/:id", controllerRouter.deleteNews)

router.post("/upload", middlewareUpload.single('image'),
                       controllerRouter.uploadImage)

module.exports = router;