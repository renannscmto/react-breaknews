import React, { useState } from "react";
import axios from "axios";
import FormInsert from "./FormInsert";

export default function Update() {
    const [title, setTitle] = useState('');
    const [news, setNews] = useState('');
    const [file, setFile] = useState([]);
    const handleDataTitle = (e) => {
            setTitle(e.target.value);
    }
    const handleDataNews = (e) => {
            setNews(e.target.value);
    }
    const handleDataImage = async (e) => {
            setFile(e.target.files[0])
    }
    const handleSendDataImage = async (e) => {
            e.preventDefault()
            const updateURL = `http://localhost:8080/upload`;
            if (!file) return;

            const dataFile = new FormData();
            dataFile.append('image', file);
            dataFile.append('title', title);
            dataFile.append('news', news);

            try {
                await axios.post(updateURL, dataFile);
            } catch (error) {
                console.error("Error ao inserir image:", error.message);
                return error.message
            }

            setTitle('');
            setNews('');
    }
    return (
        <div className="container-send">
            <h1>Insert Breaknews</h1>
            <FormInsert handleSendDataImage={handleSendDataImage} 
                        handleDataImage={handleDataImage}
                        handleDataNews={handleDataNews}
                        handleDataTitle={handleDataTitle}
                        title={title}
                        news={news} />
        </div>
    )
}