import React from "react";

import BannerHome from '../assets/banner-breaknews.png'

export default function Get() {
    return (
        <div className="container-home">
            <div className="container-text-home animate__animated animate__backInLeft animate__slow">
                <h1>Conhecimento em cada manchete. Bem-vindo ao nosso portal de notícias.</h1>
            </div>
            <div className="container-image-home animate__animated animate__backInUp animate__slow">
                <img src={BannerHome} alt="Image banner home page." />
            </div>
        </div>
    )
}