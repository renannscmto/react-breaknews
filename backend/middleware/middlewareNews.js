const validateUpdateTitle = async (req, res, next) => {
    const { title } = req.body

    if (title === undefined || title.trim() === '') {
        res.status(400).json({ message: 'Title is required and cannot be empty.' });
    } else {
        next();
    }
}

const validateUpdateText = async (req, res, next) => {
    const { news } = req.body

    if (news === undefined || news === null || news === '') {
        res.status(400).json({ message: 'News is required and must be a valid number.' });
    } else {
        next();
    }
}

module.exports = {
    validateUpdateTitle,
    validateUpdateText
}