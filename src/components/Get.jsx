import React, { useEffect, useState } from "react";
import axios from "axios";

import "font-awesome/css/font-awesome.min.css";
import FormEdit from "./FormEdit";
import RenderNews from "./RenderNews";

export default function Get() {
    const [data, setData] = useState([]);
    const [updateTitle, setUpdateTitle] = useState("");
    const [updateText, setUpdateText] = useState("");
    const [selectedId, setSelectedId] = useState(null);

    const baseURL = "http://localhost:8080/news";

    useEffect(() => {
        axios.get(baseURL).then(response => {
            setData(response.data); 
        })
    }, []) // Executado somente uma vez quando o componente for montado.

    // Função que recebe o id para chamar a api de exclusão.
    const handleDeleteNews = async (id) => {
        const baseURL = `http://localhost:8080/news/${id}`
        if(id === null) return;

        // Atualizado o estado para que o componente rederize novamente.
        const updatedData = data.filter(item => item.id_new !== id);
        setData(updatedData);

        try {
            await axios.delete(baseURL);
        } catch (error) {
            return error
        }
    }

    // Função que passa os novos valores para o estados da aplicação.
    const handleUpdateNews = (id, title, news) => {
        setSelectedId(id);
        setUpdateTitle(title);
        setUpdateText(news);
    }

    // Função que recebe os novos dados para chamar api de atualização. (enviar dados para alterar)
    const handleSendUpdate = async () => {
        const updateURL = `http://localhost:8080/news/${selectedId}`;
        if (selectedId === null) return;

        try {
            await axios.put(updateURL, {
                title: updateTitle,
                news: updateText,
            });
            // Após a atualização bem-sucedida, recarrega as notícias.
            const response = await axios.get(baseURL);
            setData(response.data);
            // Limpa os campos de edição.
            setUpdateTitle("");
            setUpdateText("");
            setSelectedId(null);
        } catch (error) {
            console.error("Error updating news:", error);
        }
    }

    return (
        <div className="container-grid">
            <h1>{selectedId == null ? 'Ultimas Notícias' : `Update Notícia: ${selectedId}`}</h1>
            <div className="grid-box">
                { selectedId !== null ? (
                    <FormEdit setUpdateTitle={setUpdateTitle} 
                              setUpdateText={setUpdateText} 
                              updateTitle={updateTitle} 
                              updateText={updateText} 
                              handleSendUpdate={handleSendUpdate}/>
                    ) : ( 
                    <RenderNews data={data} handleUpdateNews={handleUpdateNews} handleDeleteNews={handleDeleteNews}/> 
                )}
            </div>
        </div>
    )
}